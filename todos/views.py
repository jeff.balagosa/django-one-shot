from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create todo_lists view
def todo_list_list(request):
    # get all TodoList instances
    todo_lists = TodoList.objects.all()
    # context dictionary
    context = {"todo_lists": todo_lists}
    # render template
    return render(request, "todos/list.html", context)


# Create todo_list_detail view
def todo_list_detail(request, id):
    # get TodoList instance
    todo_list = TodoList.objects.get(id=id)
    # context dictionary
    context = {
        "todo_list": todo_list,
    }
    # render template
    return render(request, "todos/detail.html", context)


# Create TodoList Form View
def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # redirect to the detail view of the model
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


# Update TodoList Form View
def update_todo_list(request, id):
    # get TodoList instance
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            # redirect to the detail view of the model
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


# Delete TodoList View
def delete_todo_list(request, id):
    # get TodoList instance
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html", {"todo_list": todo_list})


# Create todo items view
def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # redirect to the detail view of the model
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/items/create.html", context)


# Update todo items view
def update_todo_item(request, id):
    # get TodoItem instance
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            # redirect to the detail view of the model
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {"form": form}

    return render(request, "todos/items/edit.html", context)
