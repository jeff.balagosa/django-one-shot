from django.forms import ModelForm
from todos.models import TodoList, TodoItem


# Create TodoListForm model form to create a new TodoList instance
class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


# Create TodoItemForm to create a new Todo item instance
class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
