from django.db import models


# TodoList model
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


# TodoItem model
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    # optional due_date
    due_date = models.DateTimeField(blank=True, null=True)
    # is_completed bool field
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(TodoList, on_delete=models.CASCADE, related_name="items")
